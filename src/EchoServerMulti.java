/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.*;
import java.io.*;
 
public class EchoServerMulti {
    public static void main(String[] args) throws IOException {
        /* 
        if (args.length != 1) {
            System.err.println("Usage: java EchoServer <port number>");
            System.exit(1);
        }
        */
        
        int portNumber = 1001;
        
        //int portNumber = Integer.parseInt(args[0]);
         
        try (
            ServerSocket serverSocket =
                new ServerSocket(portNumber);            
        ) {
            
            while (true){
                    (new Proceso(serverSocket.accept())).start();
            }
            
        } catch (IOException e) {
            System.out.println("Exception caught when trying to listen on port "
                + portNumber + " or listening for a connection");
            System.out.println(e.getMessage());
        }
    }
}

class Proceso extends Thread {
    
    private static final int INICIAL = 0;
    private static final int AUTENTICACION = 1;
    private static final int ESPERA_COMANDO = 2;
    private static final int LISTAR = 3; 
    private static final int INFO = 4; 
    private static final int BORRAR = 5; 
    private static final int FINAL = -1;
    
     Socket clientSocket;
     
     public Proceso(Socket socket){
         this.clientSocket = socket;
     }
     
     @Override
     public void run(){         
         try (    
            PrintWriter out =
                new PrintWriter(clientSocket.getOutputStream(), true);                   
            BufferedReader in = new BufferedReader(
                new InputStreamReader(clientSocket.getInputStream()));
        ) {
            
            System.out.println("Se acepto una conexion desde: "+clientSocket.getRemoteSocketAddress().toString());                
            
            String inputLine;
            String theOutput="Bienvenido al Servidor Multithreading,"+ 
                             " proporcione su usuario.";
            
            out.println(theOutput);
            
            int estado = INICIAL;
            int intentos = 1;
            
            while ((inputLine = in.readLine()) != null) {
                System.out.println(clientSocket.getRemoteSocketAddress().toString()+" - Leido : "+inputLine);
                
                if (inputLine.equals("SALIR") || intentos>2){
                    break;
                }
                
                if (estado == INICIAL) {
                    if (inputLine.equals("USUARIO FULANITO")) {
                        theOutput = "Escriba su Password";
                        estado = AUTENTICACION;
                        intentos = 1;
                    } else {
                        theOutput = "Usuario inexistente, intente de nuevo";
                        ++intentos;
                        estado = INICIAL;
                    }
                } else if (estado == AUTENTICACION){
                    if (inputLine.equals("PASSWORD PERENGANITO")){
                        theOutput = "Escriba un comando (LISTAR, INFO, BORRAR)";
                        estado = ESPERA_COMANDO;
                    } else {
                        theOutput = "Usuario o password incorrecto, " +
                        "Intente de nuevo";
                        estado = AUTENTICACION;
                        ++intentos;
                    }                                    
                } else if (estado == ESPERA_COMANDO){
                    
                    if (inputLine.startsWith("LISTAR ")){                        
                        String sFolder = inputLine.substring(7);
                        theOutput = "Archivos en el folder: ";
                        try {
                            System.out.println("Obteniendo contenido de: "+sFolder);
                            File folder = new File(sFolder);
                            String[] lista = folder.list();
                            int i=0;
                            while (i<lista.length){
                                System.out.println(lista[i]);
                                theOutput+=lista[i]+",";                                
                                ++i;
                            }                        
                        } catch (Exception e){
                            System.out.println(e.getMessage());
                            theOutput = e.getMessage();                                   
                        }
                                                                                              
                        estado = ESPERA_COMANDO;
                    } else if (inputLine.startsWith("INFO ")){
                        theOutput = "Se recibio el comando info";
                        estado = ESPERA_COMANDO;                                        
                    } else if (inputLine.startsWith("BORRAR ")){
                        theOutput = "Se recibio el comando borrar";
                        estado = ESPERA_COMANDO;                                        
                    } else {
                        theOutput = "Comando incorrecto, comandos LISTAR, INFO y BORRAR.";
                        estado = ESPERA_COMANDO;                        
                    }    
                                        
                }
                out.println(theOutput);
            }
            clientSocket.close();
        } catch (IOException e) {
            System.out.println("Exception caught read from socket");
            System.out.println(e.getMessage());
        }                
     }
}

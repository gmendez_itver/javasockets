import java.net.*;
import java.io.*;
 
public class KKMultiServerThread extends Thread {
    private Socket socket = null;
 
    public KKMultiServerThread(Socket socket) {
        super("KKMultiServerThread");
        this.socket = socket;
    }
     
    public void run() {
 
        try (
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(
                new InputStreamReader(
                    socket.getInputStream()));
        ) {
            String addrRemote = this.socket.getInetAddress().getHostAddress();
            System.out.println("Nueva conexion desde: "+addrRemote);
            String inputLine, outputLine;
            KnockKnockProtocol kkp = new KnockKnockProtocol();
            outputLine = kkp.processInput(null);
            out.println(outputLine);
 
            while ((inputLine = in.readLine()) != null) {
                System.out.println("Cliente("+addrRemote+") -> Server = "+outputLine);
                outputLine = kkp.processInput(inputLine);
                out.println(outputLine);
                System.out.println("Server -> Cliente("+addrRemote+") = "+outputLine);
                if (outputLine.equals("Bye"))
                    break;
            }
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
